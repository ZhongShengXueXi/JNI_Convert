cmake_minimum_required(VERSION 3.4.1)

set(lib_dir ${CMAKE_SOURCE_DIR}/libs)
set(inc_dir ${lib_dir}/inc)
set(lib_lib_dir ${lib_dir}/lib)

include_directories(${inc_dir})

add_library( native-lib
             SHARED
             src/main/cpp/native-lib.cpp )

add_library(libmpbase SHARED IMPORTED)
set_target_properties(libmpbase PROPERTIES IMPORTED_LOCATION ${lib_lib_dir}/${ANDROID_ABI}/libmpbase.so)
add_library(libbeautyshot SHARED IMPORTED)
set_target_properties(libbeautyshot PROPERTIES IMPORTED_LOCATION ${lib_lib_dir}/${ANDROID_ABI}/libarcsoft_beautyshot.so)

find_library( log-lib
              log )

target_link_libraries( native-lib
                       libmpbase
                       libbeautyshot
                       ${log-lib} )